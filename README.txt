# ABOUT


THIS PROJECT HAVE TWO SEPARATE PAGES FOR INTERNET SHOP - PRODUCT LIST PAGE AND PRODUCT ADD PAGE. 




## WHAT SOFTWARE YOU WILL NEED TO OPEN MY PROJECT ON YOUR OPERATE SYSTEM (OS) IN WEB BROWSER


DOCKER(VIRTUAL SERVER), VISUAL STUDIO CODE(TEXT EDITOR)




## INSTALLATION


   VISUAL STUDIO CODE SETUP


   INSTALL VISUAL STUDIO CODE ON YOUR COMPUTER: 
   code.visualstudio.com/download
   AFTER THAT OPEN THAT TEXT EDITOR.

   DOCKER SETUP


THIS PROJECT WORKS ON VIRTUAL MACHINE LINUX SYSTEM. IF YOU WANT TO SEE HOW WORK THIS PROJECT PLEASE READ RECOMANDATIONS WHAT DO YOU NEED TO INSTALL BEFORE.


INSTALL DOCKER CONTAINER TO CREATE LINUX OPERATE SYSTEM VIRTUAL MACHINE.


WHAT IS DOCKER?
DOCKER IS A SET OF PLATFORM AS A SERVICE (PAAS) PRODUCTS THAT USES OS-LEVEL VIRTUALIZATION TO DELIVER SOFTWARE IN PACKAGES CALLED CONTAINERS. CONTAINERS ARE ISOLATED FROM ONE ANOTHER AND BUNDLE THEIR OWN SOFTWARE, LIBRARIES AND CONFIGURATION FILES; THEY CAN COMMUNICATE WITH EACH OTHER TROUGH WELL-DEFINED CHANNELS. DOCKER CAN PACKAGE AN APPLICATION AND ITS DEPENDENCIES IN A VIRTUAL CONTAINER THAT CAN RUN ON ANY LINUX SERVER. THIS HELPS PROVIDE FLEXIBILITY AND PORTABILITY ENABLING THE APPLICATION TO BE RUN IN VARIOUS LOCATIONS. BECAUSE DOCKER CONTAINERS ARE LIGHTWEIGHT, A SINGLE SERVER OR VIRTUAL MACHINE CAN RUN SEVERAL CONTAINERS SIMULTANEOUSLY. DOCKER CONTAINERS CAN RUN ON ANY PLATFORM.


SUPPORTED PLATFORMS
DOCKER ENGINE IS AVAILABLE ON A VARIETY OF LINUX PLATFORMS, MAC0S AND WINDOWS 10 THROUGH DOCKER DESKTOP


STEPS FOR DOCKER INSTALLATION

FIRST YOU NEED REGISTER FOR A DOCKER ID
YOUR DOCKER ID BECOMES YOUR USER NAMESPACE FOR HOSTED DOCKER SERVICES, AND BECOMES YOUR USERNAME ON THE DOCKER FORUMS.

1. GO TO DOCKER HUB SIGNUP PAGE HUB - hub.docker.com/signup/
2. ENTER A USERNAME THAT IS ALSO YOUR DOCKER ID.
   YOUR DOCKER ID MUST BE BETWEEN 4 AND 30 CHARACTERS LONG, AND CAN ONLY CONTAIN NUMBERS AND LOWERCASE LETTERS.
3. ENTER A UNIQUE, VALID EMAIL ADDRESS.
4. ENTER A PASSWORD BETWEEN 6 AND 128 CHARACTERS LONG.
5. CLICK SIGN UP.
   DOCKER SENDS A VERIFICATION EMAIL TO THE ADDRESS YOU PROVIDED.
6. CLICK THE LINK IN THE EMAIL TO VERIFY YOUR ADDRESS.
NOTE: YOU CANNOT LOG IN WITH YOUR DOCKER ID UNTIL YOU VERIFY YOUR EMAIL ADDRESS.

LOG IN
ONCE YOU REGISTER AND VERIFY YOUR DOCKER ID EMAIL ADDRESS, YOU CAN LOG 
LOG IN TO - hub.docker.com 

INSTALL DOCKER DESKTOP ON YOUR OPERATE SYSTEM
NOW GO TO 
docs.docker.com/engine/install/#server TO INSTALL DOCKER ENGINE. HERE YOU HAVE FIND RECOMANDATIONS HOW TO INSTALL DOCKER ENGINE ON YOUR OPERATE SYSTEM(OS)

QUICK URL FOR INSTALL DOCKER DESKTOP ON MAC:
docs.docker.com/docker-for-mac/install/

QUICK URL FOR INSTALL DOCKER DESKTOP ON WINDOWS:
docs.docker.com/docker-for-windows/install/


IN MY FOLDER 'FULL-FOLDER' YOU WILL FIND FOLDER 'src' (IN THIS FOLDER IS WORKING FOLDER 'TEST-TASK' WITH PROJECT) AND 2 FILES FOR DOCKER PROPER WORK:
docker-compose.yml
Dockerfile


WHAT'S NEXT?

OPEN MY FOLDER 'FULL-FOLDER' IN YOUR TEXT EDITOR.
IN TEXT EDITOR OPEN NEW TERMINAL AND IN THE TERMINAL WRITE:
docker --version
(FOR CHECK IF DOCKER INSTALLATION IS SUCCESSFUL)

NOTE: AFTER EVERY TYPED COMMAND ALWAYS PRESS 'ENTER'
      WHEN YOU HAVE INSTALLED DOCKER YOUR OS MAY ASK PERMISSION TO RUN DOCKER SOFTWARE.

IF DOCKER INSTALLATION IS SUCCESSFUL YOU WILL SEE DOCKER VERSION
EXAMPLE: 
Docker version 18.09.7, build 2d0083d

IF DOCKER INSTALLATION NOT SUCCESSFUL YOU WILL SEE RED WARNING TEXT WITH ERRORS IN THE TERMINAL

DO YOU SEE YOUR DOCKER VERSION? VERY GOOD. NEXT WRITE IN THE TERMINAL SECOND:
docker-compose up --build
(ONLY ONE TIME FOR CONFIGURATION)

IF YOU SEE RED ERRORS DON'T WORRY. OPEN FILE IN FOLDER:
docker-compose.yml
IN THIS FILE FIND ROW volumes: (9 ROW)
ON SECOND ROW YOU WILL SEE DIRECTION TO WORKING FOLDER:
- C:/Users/User/Desktop/Practice/src:/var/www/html/
IN YOUR MACHINE IT WILL BE DIFERENT. NAVIGATE TO SHOW DIRECTION TO WORKING FOLDER (WHERE 'FULL-FOLDER' IS ON YOUR MACHINE).
EXAMPLE:
- C:/.../.../.../FULL-FOLDER/src:/var/www/html/
OR
- /.../.../.../FULL-FOLDER/src:/var/www/html/
THEN SAVE CHANGES IN THIS FILE

THEN IN TEXT EDITOR FIND ICON Extensions (ON TEXT EDITOR LEFT SIDE). OPEN THIS AND IN SEARCH WINDOW WRITE docker
INSTALL THIS EXTENSION.
WHEN IT IS INSTALLED CORRECTLY YOU WILL SEE FISH ICON IN THE TERMINAL LEFT SIDE UNDER ICON Extensions. OPEN THIS FISH ICON. IN LEFT SIDE UNDER CONTAINERS YOU WILL SEE SOME CONTAINERS web-bootcamp, mysql:8.0 AND phpmyadmin/phpmyadmin . IT MEANS THAT YOU ARE ON THE RIGHT WAY. THEY SHOULD BE GREEN (WHEN START/ON - GREEN, WHEN STOP - RED).

docker-compose up --build WITHOUT ERRORS? IF SO THEN WRITE IN THE TERMINAL:
docker-compose up
(TO START CONTAINER WORK WHEN IT'S RED)
OR START THROUGH TEXT EDITOR VISUAL INTERFACE (PRESS ON CONTAINER AND THEN RIGHT BUTTON ON MOUSE - START)


SECOND STEP

OPEN YOUR WEB BROWSER AND TYPE SECOND URL INTO THE ADDRESS BAR TO NAVIGATE TO 'TEST-TASK' PROJECT:
localhost:8000/TEST-TASK/list.php




## DATABASE (BROWSER PHPMYADMIN DATABASE)


TO SEE MY DATABASE web-bootcamp-db OPEN YOUR WEB BROWSER AND TYPE SECOND URL INTO THE ADDRESS BAR:
localhost:8083


DATA FOR ENTER

SERVER:   mysql-server-80 
USERNAME: root
PASSWORD: root_password




